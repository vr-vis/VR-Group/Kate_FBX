# Kate FBX

**Character Model**

Character model KATE, based on Kate Marsh from "Life is Strange" by Square Enix, used as embodied conversational agent (ECA) in the study of the following paper:

> Andrea Bönsch, David Hashem, Jonathan Ehret, and Torsten W. Kuhlen. 2021. _Being Guided or Having Exploratory Freedom: User Preferences of a Virtual Agent’s Behavior in a Museum_. In IVA ’21: Proceedings of the 21th ACM International Conference on Intelligent Virtual Agents (IVA ’21), September 14–17, 2021, Virtual Event, Kyoto, Japan. ACM, New York, NY, USA, 8 pages

---

**License**

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
Please find all details here: https://creativecommons.org/licenses/by/4.0/

---

**Origin of Model**

The used model with different clothing options was initially published by user isj819 under a Creative Commons Attribution 4.0 International License on
the following website: https://smutba.se/project/29487/

_Attention:_ This link contains Adult/NSFW content, that's why we provide the clothed model as used in our study separately!
SmutBase is the sister site of SFMLab which provided models and maps or use with Source Filmmaking.

---

**Our Adaptations**

Adding an oronasal-mask to meet the expected safety guidelines and social norms in times of the COVID-19 pandemic
